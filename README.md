# HighTechStore

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 11.2.4.

# CORS 

Pour faire fonctionner le front avec le back il faut l'extension google suivante : [CORS](https://chrome.google.com/webstore/detail/allow-cors-access-control/lhobafahddgcelffkeicbaginigeejlf#:~:text=Allow%20CORS:%20Access-Control-Allow-Origin%20lets%20you,default%20(in%20JavaScript%20APIs))

# Commandes pour run le projet angular 

- npm update (pour installer les dépendances)
- ng serve (pour lancer le site internet)

NB: Si npm update ne suffit pas, faire : 
- npm install bootstrap
- npm install firebase
- ng add @ng-bootstrap/ng-bootstrap

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI Overview and Command Reference](https://angular.io/cli) page.

## Info 

Credentials pour se connecter en tant qu'admin sur le site
email: admin@asi.fr
pwd: admin123

## Intro

Ce site a été réalisé dans le cadre de la matière Architecture des Systèmes d'Information. Le but a été de créer un site web de type vitrine 
en Angular en lien avec un back end développé à l'aide JAX-RS. Ce site est une ébauche d'une boutique en ligne et présente différents produits et articles.
Pour plus d'informations, veuillez consulter notre GIT en cliquant sur l'icone ci-dessous
