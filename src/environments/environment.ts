// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig: {
    apiKey: "AIzaSyBQHuCXBehak-s6jY2MS-1rS6gJAt0K75w",
    authDomain: "asi-angular.firebaseapp.com",
    projectId: "asi-angular",
    storageBucket: "asi-angular.appspot.com",
    messagingSenderId: "403875371020",
    appId: "1:403875371020:web:3e012fb178b14d1508b629",
    measurementId: "G-37FKQZY6ZE"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
