import { Article } from "./article";


export class Categorie{
    constructor(
        public idCategorie: number,
        public libelleCategorie: string,
        public articleList: Array<Article>,
        public categorieParent?: String
      ) {  }
}