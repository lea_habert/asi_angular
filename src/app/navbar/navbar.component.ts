import { Component, OnInit } from '@angular/core';
import { AuthService } from '../services/auth.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})

export class NavbarComponent implements OnInit {

  isAdmin: boolean = false
  constructor(private auth: AuthService, private route: Router) {
   
    this.auth.user.subscribe(user => {
      
      if(user) { 
        this.isAdmin = true}
      else {
        this.isAdmin = false}
    })
   }

  ngOnInit(): void {
  }


  logOut(){
    this.isAdmin = false
    this.auth.signOut()
    this.route.navigate([''])
  }

}