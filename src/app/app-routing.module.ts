import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ConnexionComponent } from './connexion/connexion.component';
import { HomeComponent } from './home/home.component';
import { PhonesAccessoiresComponent } from './modules/phones/phones-accessoires/phones-accessoires.component';
import { SmartPhoneComponent } from './modules/phones/smart-phone/smart-phone.component';
import { TelFixeComponent } from './modules/phones/tel-fixe/tel-fixe.component';
import { PcAccessoiresComponent } from './modules/ordinateurs/pc-accessoires/pc-accessoires.component';
import { PcBureauComponent } from './modules/ordinateurs/pc-bureau/pc-bureau.component';
import { PcPortableComponent } from './modules/ordinateurs/pc-portable/pc-portable.component';
import { CleUsbComponent } from './modules/stockage/cle-usb/cle-usb.component';
import { DisqueDurComponent } from './modules/stockage/disque-dur/disque-dur.component';
import { StockageAccessoiresComponent } from './modules/stockage/stockage-accessoires/stockage-accessoires.component';
import { DashboardComponent } from './modules/admin/dashboard/dashboard.component';
import { NoauthGuard } from './guards/noauth.guard';
import { AuthGuard } from './guards/auth.guard';

const routes: Routes = [
  { path: 'Connexion', component: ConnexionComponent, canActivate: [NoauthGuard] },
  { path: '', component: HomeComponent},
  { path: 'Ordinateurs', children: [
    { path: 'Accessoires', component: PcAccessoiresComponent},
    { path: 'Fixe', component: PcBureauComponent},
    { path: 'Portable', component: PcPortableComponent},
  ]},
  { path: 'Phones', children: [
    { path: 'Portable', component: SmartPhoneComponent},
    { path: 'Fixe', component: TelFixeComponent},
    { path: 'Accessoires', component: PhonesAccessoiresComponent},
  ]},
  { path: 'Stockage', children: [
    { path: 'Usb', component: CleUsbComponent},
    { path: 'Disk', component: DisqueDurComponent},
    { path: 'Accessoires', component: StockageAccessoiresComponent},
  ]},

  { path: 'Admin', children: [
    { path: 'Dashboard', component: DashboardComponent},
  ], canActivate: [AuthGuard]}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
