export class Article{
    
    constructor(
        public idArticle: number,
        public libelleArticle: string,
        public prix: number,
        public photo?: string,
        public categorie?: String
      ) {  }
}