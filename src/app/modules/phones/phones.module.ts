import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PhonesRoutingModule } from './phones-routing.module';
import { SmartPhoneComponent } from './smart-phone/smart-phone.component';
import { TelFixeComponent } from './tel-fixe/tel-fixe.component';
import { PhonesAccessoiresComponent } from './phones-accessoires/phones-accessoires.component';


@NgModule({
  declarations: [SmartPhoneComponent, TelFixeComponent, PhonesAccessoiresComponent],
  imports: [
    CommonModule,
    PhonesRoutingModule
  ]
})
export class PhonesModule { }
