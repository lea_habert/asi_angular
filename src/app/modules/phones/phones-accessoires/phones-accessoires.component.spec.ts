import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PhonesAccessoiresComponent } from './phones-accessoires.component';

describe('PhonesAccessoiresComponent', () => {
  let component: PhonesAccessoiresComponent;
  let fixture: ComponentFixture<PhonesAccessoiresComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PhonesAccessoiresComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PhonesAccessoiresComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
