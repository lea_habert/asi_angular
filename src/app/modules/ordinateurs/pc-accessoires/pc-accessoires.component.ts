import { Component, OnInit } from '@angular/core';
import { OrdinateursService } from 'src/app/services/ordinateurs.service';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-pc-accessoires',
  templateUrl: './pc-accessoires.component.html',
  styleUrls: ['./pc-accessoires.component.css']
})
export class PcAccessoiresComponent implements OnInit {

  list: Array<String> | any;
  listArticles: Array<String> | any;
  detailsArticle: String | any;
  Article: Array<String> | any;

  constructor(private ordiService: OrdinateursService, private modalService: NgbModal) { }

  ngOnInit(): void {
    this.ordiService.getAll("4").subscribe(result => {
      this.list = result;
    }); //appel à cette méthode déclarée dans le service, qui permet de récuprer les données de l'url donnée
    
    this.ordiService.getAllArticles().subscribe(result => {
      this.listArticles = result;
    });
  }

  openDetailsArticle(targetModal: any, idArticle: String) {
    this.detailsArticle = idArticle;
    this.ordiService.getDetailsArticle(this.detailsArticle).subscribe(result =>
      this.Article=result)
    this.modalService.open(targetModal, {
      backdrop: 'static',
      size: 'lg'
    });
  } 
}
