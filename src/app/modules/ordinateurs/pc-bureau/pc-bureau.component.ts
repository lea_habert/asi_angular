import { Component, OnInit } from '@angular/core';
import { OrdinateursService } from 'src/app/services/ordinateurs.service';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-pc-bureau',
  templateUrl: './pc-bureau.component.html',
  styleUrls: ['./pc-bureau.component.css']
})
export class PcBureauComponent implements OnInit {


  detailsArticle: String | any;
  listArticles: Array<String> | any;
  list: Array<String> | any;
  Article: Array<String> | any;

  constructor(private ordiService: OrdinateursService, private modalService: NgbModal) { }

  ngOnInit(): void {
    this.ordiService.getAll("3").subscribe(result => {
      this.list = result;
    }); //appel à cette méthode déclarée dans le service, qui permet de récuprer les données de l'url donnée

    this.ordiService.getAllArticles().subscribe(result => {
      this.listArticles = result;
    });
  }
  openDetailsArticle(targetModal: any, idArticle: String) {
    this.detailsArticle = idArticle;
    this.ordiService.getDetailsArticle(this.detailsArticle).subscribe(result =>
      this.Article=result)
    this.modalService.open(targetModal, {
      backdrop: 'static',
      size: 'lg'
    });
  } 
}
