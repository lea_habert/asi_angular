import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { AdminService } from 'src/app/services/admin.service';

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type' : 'application/x-www-form-urlencoded'})
} 

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})


export class DashboardComponent implements OnInit {

  deleteCategorieId: String | any;
  updateCategorieId: String | any;
  deleteArticleId: String | any;
  updateArticleId: String | any;

  listCategories: Array<String> | any;
  Categorie: Array<String> | any;
  listArticles: Array<String> | any;
  Article: Array<String> | any;

  resultPost: String | any;
  resultPut: String | any;

  constructor(private adminService: AdminService, private http: HttpClient, private modalService: NgbModal) { }

  ngOnInit(): void {
    this.adminService.getAllCategories().subscribe(result => {
      this.listCategories = result;
    }); //appel à cette méthode déclarée dans le service, qui permet de récuprer les données de l'url donnée

    this.adminService.getAllArticles().subscribe(result => {
      this.listArticles = result;
    });
  }

  openUpdateCategorie(targetModal: any, idCategorie: String) {
    this.updateCategorieId = idCategorie;
    this.adminService.getDetailsCategorie(this.updateCategorieId).subscribe(result =>
      this.Categorie=result)
    this.modalService.open(targetModal, {
      backdrop: 'static',
      size: 'lg'
    });
  }

  openDeleteCategorie(targetModal: any, idCategorie: String) {
    this.deleteCategorieId = idCategorie;
    this.modalService.open(targetModal, {
      backdrop: 'static',
      size: 'lg'
    });
  }

  /* changeCategorie(){
    const changeUrl = 'http://localhost:8080/asi/Boutique/categories/'+this.changeCategorieId+'/change';
    this.http.delete(changeUrl).subscribe(result =>
      this.ngOnInit)
  } */

  deleteCategorie(){
    const deleteUrl = 'http://localhost:8080/asi/Boutique/categories/'+this.deleteCategorieId+'/delete';
    this.http.delete(deleteUrl).subscribe(result =>
      this.ngOnInit)
  }

  openUpdateArticle(targetModal: any, idArticle: String) {
    this.updateArticleId = idArticle;
    this.adminService.getDetailsArticle(this.updateArticleId).subscribe(result =>
      this.Article=result)
    this.modalService.open(targetModal, {
      backdrop: 'static',
      size: 'lg'
    });
  }

  openDeleteArticle(targetModal: any, idArticle: String) {
    this.deleteArticleId = idArticle;
    this.modalService.open(targetModal, {
      backdrop: 'static',
      size: 'lg'
    });
  }

  /* changeArticle(){
    const changeUrl = 'http://localhost:8080/asi/Boutique/articles/'+this.changeArticleId+'/change';
    this.http.delete(changeUrl).subscribe(result =>
      this.ngOnInit)
  } */

  deleteArticle(){
    const deleteUrl = 'http://localhost:8080/asi/Boutique/articles/'+this.deleteArticleId+'/delete';
    this.http.delete(deleteUrl).subscribe(result =>
      this.ngOnInit)
  }

  openAjoutArticle(content: any) {
    this.modalService.open(content, {ariaLabelledBy: 'modal-basic-title'});
  }

  openAjoutCategorie(content: any) {
    this.modalService.open(content, {ariaLabelledBy: 'modal-basic-title'});
  }

  // ajoutArticle(ajoutArticleForm: any) {
  //   const url = 'http://localhost:8080/asi/Boutique/articles';
  //   this.http.post(url, ajoutArticleForm.value)
  //     .subscribe((result) => {
  //       this.ngOnInit();
  //     });
  // }

  /* ajoutArticle(ajoutArticleForm:any) {

    console.log(ajoutArticleForm)
    this.adminService.postArticle(ajoutArticleForm.value).subscribe(result => 
      this.resultPost = result);
    console.log(this.resultPost)
  }
 */
  ajoutArticle(articleForm: any){
    // postArticle(): Observable<Article> {
    let data = articleForm
    console.log(articleForm)

    console.log(data.id)
    let id: string = data.id
    let prix: string = data.prix
    let photo: string = data.photo
    let libelle: string = data.libelle
    let categorie: string = data.categorie

  
    const body = new HttpParams()
    .append('id', id)
    .append('prix', prix)
    .append('photo', photo)
    .append('libelle', libelle)
    .append('categorie', categorie)
    //let data = ajoutArticleForm
    console.log(body)
    this.http.post("http://localhost:8080/asi/Boutique/articles", body, httpOptions).subscribe(result => 
    this.resultPost = result);
    console.log(this.resultPost);
  }

  updateArticle(updateArticleForm: any, idArticle: any){
    let data = updateArticleForm
    console.log(updateArticleForm)

    let id: string = idArticle
    let prix: string = data.prix
    let photo: string = data.photo
    let libelle: string = data.libelle
    let categorie: string = data.categorie

    const body = new HttpParams()
    .append('id', id)
    .append('prix', prix)
    .append('photo', photo)
    .append('libelle', libelle)
    .append('categorie', categorie)
    //let data = ajoutArticleForm
    console.log(body)
    this.http.post("http://localhost:8080/asi/Boutique/articles", body, httpOptions).subscribe(result => 
    this.resultPut = result);
    console.log(this.resultPut);
  }

  /* ajoutArticle(){
    this.adminService.postArticle().subscribe(result => {
      this.resultPost = result;
      console.log(this.resultPost)
    })
  } */
  ajoutCategorie(categorieForm: any){
    // postArticle(): Observable<Article> {
    let data = categorieForm
    console.log(categorieForm)

    console.log(data.id)
    let id: string = data.id
    let libelle: string = data.libelle
    let categorieParent: string = data.categorieParent

    const body = new HttpParams()
    .append('id', id)
    .append('libelle', libelle)
    .append('categorieParent', categorieParent)
    console.log(body)
    this.http.post("http://localhost:8080/asi/Boutique/categories", body, httpOptions).subscribe(result => 
    this.resultPost = result);
    console.log(this.resultPost);
  }

  updateCategorie(categorieForm: any, idCategorie: any){
    // postArticle(): Observable<Article> {
    let data = categorieForm
    console.log(categorieForm)

    let id: string = idCategorie
    let libelle: string = data.libelle
    let categorieParent: string = data.categorieParent

    const body = new HttpParams()
    .append('id', id)
    .append('libelle', libelle)
    .append('categorieParent', categorieParent)
    console.log(body)
    this.http.post("http://localhost:8080/asi/Boutique/categories", body, httpOptions).subscribe(result => 
    this.resultPost = result);
    console.log(this.resultPost);
  }


  submitted = false;

  onSubmit() { this.submitted = true; }

}
