import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { OrdinateursService } from 'src/app/services/ordinateurs.service';

@Component({
  selector: 'app-cle-usb',
  templateUrl: './cle-usb.component.html',
  styleUrls: ['./cle-usb.component.css']
})
export class CleUsbComponent implements OnInit {

  list: Array<String> | any;
  detailsArticle: String | any;
  Article: Array<String> | any;

  constructor(private ordiService: OrdinateursService, private http: HttpClient, private modalService: NgbModal) { }

  ngOnInit(): void {
    this.ordiService.getAll("11").subscribe(result => {
      this.list = result;
    }); //appel à cette méthode déclarée dans le service, qui permet de récuprer les données de l'url donnée
  }

  openDetailsArticle(targetModal: any, idArticle: String) {
    this.detailsArticle = idArticle;
    this.ordiService.getDetailsArticle(this.detailsArticle).subscribe(result =>
      this.Article=result)
    this.modalService.open(targetModal, {
      backdrop: 'static',
      size: 'lg'
    });
  }
}
