import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CleUsbComponent } from './cle-usb/cle-usb.component';
import { DisqueDurComponent } from './disque-dur/disque-dur.component';
import { StockageAccessoiresComponent } from './stockage-accessoires/stockage-accessoires.component';

const routes: Routes = [
  { path: 'DisqueDur', component: DisqueDurComponent},
  { path: 'CleUSB', component: CleUsbComponent},
  { path: 'StockageAccessoires', component: StockageAccessoiresComponent}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class StockageRoutingModule { }
