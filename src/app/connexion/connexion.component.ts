import { Component, OnInit } from '@angular/core';
import { AuthService } from '../services/auth.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-connexion',
  templateUrl: './connexion.component.html',
  styleUrls: ['./connexion.component.css']
})
export class ConnexionComponent implements OnInit {

  messageError : string = ''
  isLoginSuccess = false

  constructor(private auth: AuthService, private route: Router) {}

  ngOnInit(): void {

    console.log(this.isLoginSuccess)
  }
  connexion(connexionForm: any){
    let data = connexionForm.value
    console.log(connexionForm.value)
    this.auth.signIn(data.email, data.pass)
      .then(() => {

        console.log("Connecté !");
       this.route.navigate(['']);

      }).catch(() => {

      console.log("Erreur connexion");
      this.messageError="Erreur d'authentification"

    })
  }
}



