import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http'

@Injectable({
  providedIn: 'root'
})
export class OrdinateursService {

  //url = 'http://localhost:3000/articles';
  //url = 'http://localhost:8080/asi/Boutique/articles';
  //url = 'https://202db496e1c2.ngrok.io/asi/Boutique/articles';
  urlCategorie = 'http://localhost:8080/asi/Boutique/categories/';
  urlArticles = 'http://localhost:8080/asi/Boutique/articles/';

  constructor(private http: HttpClient) { }

  getAll(idCategorie: String): Observable<Array<String>>{
    var urlCat = this.urlCategorie+idCategorie;
    return this.http.get<Array<String>>(urlCat);
  }

  getAllArticles(): Observable<Array<String>>{
    return this.http.get<Array<String>>(this.urlArticles);
  }

  /* updateArticle(idArticle: String): Observable<Array<String>>{
    return this.http.put()
  } */

  getDetailsArticle(idArticle: String): Observable<Array<String>>{
    var urlid = this.urlArticles+idArticle;
    return  this.http.get<Array<String>>(urlid);
  }
}
