import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http'
import { Article } from '../article';

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type' : 'application/x-www-form-urlencoded'})
} 

@Injectable({
  providedIn: 'root'
})



export class AdminService {

  idCategorie: String | any;
  idArticle: String | any;

  urlCategories = 'http://localhost:8080/asi/Boutique/categories/';
  urlArticles = 'http://localhost:8080/asi/Boutique/articles/';
  urlArticle = 'http://localhost:8080/asi/Boutique/articles/';

  constructor(private http: HttpClient) { }

  getAllCategories(): Observable<Array<String>>{
    return this.http.get<Array<String>>(this.urlCategories);
  }

  getAllArticles(): Observable<Array<String>>{
    return this.http.get<Array<String>>(this.urlArticles);
  }

  getDetailsArticle(idArticle: String): Observable<Array<String>>{
    var urlid = this.urlArticles+idArticle;
    return  this.http.get<Array<String>>(urlid);
  }

  getDetailsCategorie(idCategorie: String): Observable<Array<String>>{
    var urlid = this.urlCategories+idCategorie+"/details";
    return  this.http.get<Array<String>>(urlid);
  }

  /* postArticle(valueForm: any): Observable<Article> {
  // postArticle(): Observable<Article> {
    console.log(valueForm.idArticle)
    let id: string = valueForm.idArticle
    let prix: string = valueForm.prix
    let photo: string = valueForm.photo
    let libelleArticle: string = valueForm.libelleArticle
    let categorie: string = valueForm.categorie

    const body = new HttpParams()
    .set('idArticle', id)
    .set('prix', prix)
    .set('photo', photo)
    .set('libelle', libelleArticle)
    .set('categorie', categorie)
    //let data = ajoutArticleForm
    console.log(body)
    return this.http.post(this.urlArticles, body, httpOptions);
  } */
}