import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http'

@Injectable({
  providedIn: 'root'
})
export class FooterService {

  url = 'http://localhost:8080/asi/Boutique/boutique/';

  constructor(private http: HttpClient) { }

  getAll(): Observable<Array<String>>{
    var urlBout = this.url;
    return this.http.get<Array<String>>(urlBout);
  }
}
