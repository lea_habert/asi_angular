import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { AngularFireAuth } from "@angular/fire/auth";

import { Observable } from 'rxjs';

const url = 'http://localhost:5555/'

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type' : 'application/json'})
} 

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  user: Observable<any>  
  constructor(private fa: AngularFireAuth) {
    this.user = this.fa.user
   }

  signIn(email: string, pass: string) {
    console.log(this.fa.signInWithEmailAndPassword(email, pass))
    return this.fa.signInWithEmailAndPassword(email, pass).then(() => {
      //this.isSignedIn= true
      console.log('Connexion firebase effectuée ! ')
    }).catch((error) => {
      console.log("erreur "+error)
    })
  }

  signOut(): any{
    console.log(this.fa.signOut())
    return this.fa.signOut().then(() => {
     //this.isSignedIn = false
      console.log("logout ")
    }).catch((error) => {
      console.log("erreur : "+error)
    }
    );
  }
}
