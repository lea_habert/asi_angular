import { Component, OnInit } from '@angular/core';
import { FooterService } from '../services/footer.service';

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.css']
})
export class FooterComponent implements OnInit {

  list: Array<String> | any;

  constructor(private ordiService: FooterService) { }

  ngOnInit(): void {
    this.ordiService.getAll().subscribe(result => {
      this.list = result;
    }); //appel à cette méthode déclarée dans le service, qui permet de récuprer les données de l'url donnée
  }

}
